% \documentclass[11pt,letterpaper]{article}
\documentclass{6046}
\title{6.829 - Computer Networks \\ Lecture 5 - Router Design}

\author{Victor Costan, Rati Gelashvili}
\problemset{5}
\date{20 Feb 2013}

\begin{document}

\section*{Routers and Algorithms}

Routers are what makes networks work, and they have evolved to become bigger
and faster with growing demand. Apart from a few academic groups, the bulk
of router innovation is carried out in the industry. Nevertheless, the main
ideas do come from universities. Stanford is leading in this regard, having a
very strong group in network hardware design. the ideas discussed below have
originated at Stanford.

iSLIP is an algorithm that has tremendous practical impact. Remarkably, it is
one of the few most common algorithms used in the routers, up until about
five years ago, all Cisco routers used it. It's unlikely that sending a packet
over the Internet will not cause it to hit at least one router that uses iSLIP.
iSLIP[1] is a decent algorithm, not necessarily the best, but it found the way
to the industry easily when its author from Stanford worked for Cisco for a
year. Cisco also ended up licensing the patent.

\begin{figure}[hbtp]
  \hspace{-75mm}\center{\includegraphics[width=140mm]{figs/router}}
  \caption{
    General router design example, 10-port router that has 5 line cards
    with two ports on each card.
  }
  \label{fig:router}
\end{figure}

A router has a number of line cards. Each line cards has some ports, and the
links are attached to these ports. Each link is bi-directional, so each link
maps to an ingress port (packets come in) and an egress port (packets go out).
Figure \ref{fig:router} is an example of a 10-link router with 5 line cards.
Each link is bi-directional, so the router in the picture has 10 ingress
(input) ports and 10 egress (output) ports. Routers today are 128x128 or
256x256 and contain enormous amounts of RAM.

Packets come in on links and go out on other links. In theory, it is even
possible to have a packet go in and come out on the same link (although, this
would be a peculiar behavior). Over time, the link speeds have been increasing.
When the iSLIP paper[1] was written, the links were 2Gpbs, to put this into
proscpective, today each computer has a 1 Gbps link.

There also exists a MIT company that makes routers, Sandburst. Broadcom uses
their ideas on their low-end routers that compete with Cisco.

\section*{Router Design}

Here are several important aspects that need to be considered about the router
design:

\begin{itemize}

\item Inside any router, there has to be a network connecting the line-cards.
How does the interconnection network look like?

\item The routers maintain buffers. Where are the buffers physically? How big
are they?

\item Where and how the link scheduling work?

\item Finally, arbitration: once you have topology connecting line cards, how
are the connections shuffled?

\end{itemize}

\subsubsection*{Shared-Bus Design}

The first routers had one shared bus, used by the connections from any line
card to any other. Assuming there are $N$ links in the router and that all
links have the same speed $s$, there can be $Ns$ packets on the shared bus in
a single time slot. In order for the shared bus design to work, it needs to be
fast, so that the network inside the router does not become the bottleneck,
meaning that the inside bus should be $N$ times faster than the line speed,
which is infeasible with large $N$. It used possible to buy a PC with bus
having $6x$ or $8x$ line speed, but not much more.

\textbf{The router speed-up is the fastest speed at which two line cards can
communicate with each other, divided by the speed of a link.} Hence, if the
internal bus speed is $N$ times the link speed, the router is said to have a
speed-up of $N$.

A router speed-up of $N$ essentially means that no input buffering is necessary
and that all the buffering can be done at the output. Incoming packets go to
the egress, and the only problem left is scheduling packets at egress.
The problem as also mentioned above, is coming up with the internal topology
with a speed-up of $N$. The shared bus doesn't work because it's impossible to
scale the internal network with high link speed and high number of links.

\subsubsection*{Crossbar Design}

The shared bus design doesn't work due to scaling. A very popular altenative
design is the crossbar, shown in figure \ref{fig:crossbar}, which is probably
the cheapest existing interconnection technology.

\begin{figure}[hbtp]
  \hspace{-75mm}\center{\includegraphics[width=60mm]{figs/crossbar}}
  \caption{
    2x2 crossbar. The top ingress port is connected to the left egress port,
    and the bottom ingress port is connected to the right egress port. Each
    ingress port must be connected to at most one egress port, and each egress
    port must be connected to at most one ingress port, otherwise electrical
    chaos occurs.
  }
  \label{fig:crossbar}
\end{figure}

A crossbar is a mesh connecting ingress ports to egress ports, switches
closing the circuits between ingress and egress ports, where at most one switch
per row / column can be closed. Now routers have layered crossbars: a
crossbar on a layer, another one on another layer, achieving a speed-up of two.

Usually, the power and error rate requirements are stricter for the internal
links (for several reasons, one being that no producer wants its router to be
the weak link on the network, also there is no CRC inside the switch). Therefore,
building the internal links for the router can be just as, or even harder than
building the outside links. For instance, Sandburst produced routers with 3 Gbps
inside links for 10 Gbps outside links.

It is feasible to build arbiters that run in every time slot - packets come in,
get broken into fixed-size (or max-size) cells and the cells get shipped across
the inside network in time slots. The arbiters that decide what gets switched
are common, so building them is cost-effective. The simple crossbar topology is
also very easy to simulate (bi-partite graph, matchings computed in each time
slot). Complex topologies would be harder to simulate and have a risk that
simulation results won't match the reality.

The loads on the links are not uniform and this non-uniform loads are harder to
deal with. For example if everyone wants to talk to Facebook, there will be a
lot of contention for one link. If loads were uniform, they would be really easy
to schedule in a crossbar.

\section*{Arbitration}

The high-level task is to find a bipartite matching between line cards - meaning
that each input is connected to at most one output and each output is connected
to at most one input.

Let us consider more specific goals for arbitration.

i. \textbf{The arbitration should be stable.}

If the algorithm causes any starvation, a size of some queue might explode.
Stability means that there should not be a load pattern that causes an ingress
queue to baloon up infinitely. In general, stability can not be guaranteed, just
consider all the links sending traffic to Facebook (one link) at full rate. If
the router has a speed-up below $N$, it's not possible to forward all the traffic
to the output. Even with the speed up $N$, infinite queue would accumulate at the
output.

\textbf{Well-behaved traffic}: Consider an $NxN$ matrix, rows corresponding to
ingress ports and columns to the egress ports. The entry at a given row and column
denotes what fraction of the traffic on the input link goes to the corresponding
output link. In the Facebook example, all the numbers on the Facebook column are 1
and all the other numbers are 0. Intuitively, if the traffic is well-behaved, the
sum of the numbers on each row / column should be less than or equal to 1. In
particular, if the sum of numbers on a column is bigger than one, this means there
is too much traffic for an output link. A matrix with this property is called
\emph{doubly sub-stochastic}.

If the input traffic is \emph{well-behaved}, no matter what the arrival
pattern in the input traffic, the ingress queues will not grow unbounded.

ii. \textbf{The arbitration should be fair}.

If an input link has a packet waiting to reach an output, it should be scheduled
within some time. When looking over a large number of time slots (e.g., $3N$,
$4N$), there should be no starvation. iSLIP in particular has this property.

The fact that iSLIP works well has been initially demonstrated by countless
simulations, before it was formally proven that a router with a speed-up of two,
and a doubly sub-stochastic traffic matrix, as long as the router does
\emph{maximal matching} (not maximum matching), leads to a completely stable
system with no queue growth. This is a truly remarkable result. The proof is very
mathematical, detailed and out of scope, but this is why the iSLIP algorithm works.

In order to achieves $100\%$ throughput in a router with speed-up equal to one,
just matching everything possible using maximum matching is not sufficient. The
fractions (values in the router traffic matrix) also need to be considered,
therefore, it is the Maximum-weight matching, not simply maximum matching
algorithm that achieves the $100\%$ throughput mark. However, computing it has
a complexity $O(N^3)$, which is too high to be carried out in practice.

For this reason, the focus is on maximal matchings. Maximal matchings are
matchings that are built incrementally by adding new matched pairs until possible.
The size of a maximal matching is at least $1/2$ of the size of the maximum
matching, proof is very simple and is left to the reader as an exercise.

Maximal matching can be implemented in three phases that repeated until the
matching is maximal:

1. Request - Ingress has a packet for egress, asks to be connected to egress

2. Grant - Egress says ok to one of the requests

3. Accept - Ingress accepts one of the grants it receives

Computing a maximal matching requires about $log(N)$ iterations through the 3 steps.
As $N$ becomes large, even this doing this quickly is very difficult. For example,
the Sandburst routers had 49ns to make decision for a time slot. Therefore, the work
is typically pipelined and the decisions are made for ~100 time slots in the future.

There are various schemes that differ mostly in strategies for selecting which
request to ok in the second phase and which grant to accept in the third phase.
But these different schemes achieve different degrees of fairness.

One could think of a strategy to keep a rotating pointer and accept / ok in a
round-robin fashion (select first possible after the pointer, move pointer
forward). Alternatively, the \emph{PIM} scheme chooses randomly.

If the traffic is not well-behaved, we'd like arbitration algorithm to not cause
starvation. Unfortunately, these schemes do not provide fairness guarantees.
Simple round-robin can starve particular input forever, while random assignment
does not give finite bounds.

But these schemes can be tweaked to receive the iSLIP algorithm with fairness
guarantees. The first tweak / subtlety is to only move forward in the round-robin
in the second phase if and only if the match is accepted. Let us prove that with
this tweak no connection starves, i.e. there are \emph{tight fairness guarantees}:

The following invariant holds: If ingress $i$ has some packets for egress $j$ and
if $i$ is the highest priority ingress for $j$ with some packets, then, in at most
$N$ algorithm steps (each step consists of establishing a new matching), the
connection from $i$ to $j$ will be served. $i$ will always send a request to $j$,
because it has packets, and because $i$ has the highest priority for $j$, $j$ will
keep granting and will never update the pointer and priorities unless the connection
from $i$ is served. $i$ will have at least this grant at every step, thus it will
participate in a matching and keep moving its own accept pointes, which means that
in at most $N$ steps, $i$ will accept the grant from $j$ and the connection will be
served.

Now, consider arbitrary connection from ingress $i$ to egress $j$ with some packets
to send. If $i$ is the highest priority ingress with packets for $j$, then it will
be served within $N$ steps, if not, then whatever ingress is will be served and the
grant pointer of $j$ will move forward. There could have been at most $N$ ingresses
with packets for $j$ and grant priority higher than $i$, meaning that after $N^2$
steps, $i$ will reach the highest priority and will be served. Therefore, no
connection is starved and the algorithm with the tweak provides fairness guarantees
within $N^2$ steps.

The paper[1] has a nice example illustrating that doing round-robin blindly results
in arbiter synchronization which results in having only $~60\%$ efficiency. "i"
in "iSLIP" stands for "iterative" - the second tweak is doing multiple iterations
of phases, with a small number of iterations (2-4) sufficient to overcome the
problems.

There is an important aspect - we need to make sure that the fairness guarantees are
still valid. In fact, if now egress moves a pointer after a successful grant in any
round, the invariant used above for proving fairness no longer holds. Ingress $i$
could have had packets for egress $j$, $i$ could have been the top priority for $j$,
but in the first round $i$ could accept grant of enother egress, in the second round
$j$ would not receive request for $i$ (because $i$ is already matched for now) and
could get his request accepted by some ingress $k$, triggering the pointer to move
past $i$, meaning that in the next algorithm step, $i$ would not stay top priority
and thus there is no guarantee that $i$ would be served in $N$ steps.

The problem is clear, the pointer should only be moved if the grant is accepted by
an ingress that has the highest priority and some packets to send. The problem is
that requests are used to tell what are the ingresses that have packets to send to
a given egress, but in the rounds after the first round, some ingresses are matched,
so the egress may not know about all the ingresses that have packets to send to it.
To solve this, the egress $j$ could store the information about the highest priority
ingress $i$ with packets to send in the first round, and only move the pointer in
any round if it gets matched with $i$. This approach provides fairness guarantees,
but is not very practical, because it makes the rounds more dependent on each other,
and $j$ has to store more and do a lot more comparisons. Furthermore, most of this
comparisons will just indicate that pointer should not be moved.

Therefore, iSLIP designers came up with more elegant, allas somewhat
counter-intuitive algorithmic formulation that also provides the fairness guarantees
but is more practical - the pointer is only updated if the grant is accepted in the
first round. Because in the first round there is always the request from the highest
priority ingress $i$, it will never be skipped and starved and thus fairness will be
guaranteed for any connection in at most $N^2$ algorithm steps. On the other hand,
occasionally the algorithm does not move the pointer in later rounds when it could
have safely done so (if egress $j$ got matched with highest priority ingress $i$) -
this does not introduce any problems and happens rarely, on the other hand, the
algorithm is more practical and easier to formulate.

The algorithm description is nice and clear, but why it works out in the wild is not
so completely clear. For instance, as mentioned above, a proof about stability came
after the algorithm was already deployed.

iSLIP also has supports priorities using three bands (high / med / low) - the
algorithm is applied in rounds, with the high-priority packets served first and
only then the other priorities taking up the remaining available matching slots.

\section*{Buffers}

We have seen algorithms that pair up ingress with egress links, where as long as
the speed-up is two, system is stable. We have also seen the issue of fairness.
Next, let us focus on where to store the packet buffers.

Switches with all the buffering done on the input are called \emph{input-queued}.
Routers with speed-up $N$ can be \emph{output-queued}. But the routers with
speed-up less than $N$ need buffers on the input in order to survive short
(2-3 seconds) periods of not well-behaved traffic. Buffering on the outputs is
also very useful in the case when the egress links are busy.

Does this imply using twice as much memory as required? The way out is to have
large input buffers and tiny output buffer. But what about the packet scheduling
algorithms? In reality, output buffers are about $1\%$ of the input buffer size,
so they can hold several packets. Good routers implement FQ on the output buffers,
and also distribute some of the FQ work on the input buffers, so FQ has some
visibility on input buffers.

How big should the buffers be? Wisdom says bandwidth-delay product, where typical
delay is 10ms for data center routers, 100ms for Internet routers. The reason is
that a TCP connection, if the queue size is the product of the bandwidth and the
minimum RTT, will never under-utilize the link.

\begin{figure}[hbtp]
  \hspace{-75mm}\center{\includegraphics[width=160mm]{figs/buffers}}
  \caption{
    TCP connection considered by the bandwidth-delay argument. Both the sender
    and receiver are connected to edge routers via high-speed links. The
    routers are connected by a link of capacity $C$. The total round-trip time
    between the sender and receiver (the sum of RTTs on all links) is
    $\textrm{RTT}_{\min{}}$.
  }
  \label{fig:buffers}
\end{figure}

Here is a simplified argument. Consider the scenario in figure
\ref{fig:buffers}. Let $P$ "pipe size" be the bandwidth-delay product, which
is computed as $P = C \cdot \textrm{RTT}_{\min{}}$. Let $W$ be the window size.
RTTmin is the time it takes to send a packet and get the ACK back, assuming no
queueing. If the window size $W$ is bigger than $P$, then $P$ packets are on
the links, and $Q = W - P$ packets are in the queues. At some point, timeout
occurs and the window size is halved from $W$ to $W/2$ (the TCP sawtooth
pattern). In order for the link not to starve number of packets in queue should 
never go to $0$ for a long period of time. This implies that $W/2$ should still 
be greater than or equal to $P$, so $W \geq 2P$. Now, as $Q = W - P \geq P$, $Q$ 
needs to be at least $P$, the bandwidth-delay product, in order to never
under-utilize the link. In particular, $Q = P$ is the smallest buffer size
that has this property.

Do we put a bandwidth-delay product of buffering for each output link on each
input buffer? How do we spread this bandwidth-delay product of buffering between
inputs? One could have intermediate memory in the router, but this would make
things slow and consume precious inner bandwidth.

Proper solution is not to think of output bandwidth-delay, but about the input
bandwidth-delay products, and have the corresponding input buffer sizes. There
can not be more output than input, so the actual output bandwidth-delay product
buffer will be available.

The amount of buffering on the output is again a tradeoff: Too little buffering
means links can starve. Too much means wasted money and delayed packets, because
TCP fills any available buffer. Because the memory is chip, and because there
exists a bias in router benchmarks favoring the implementations that would rather
over-saturate the link introducing higher delays to maintain the throughput, the
buffer size is usually set relatively large. It is safer to over-provision than to
under-provision. For example, Sandburst used simulations to find some results,
multiplied it by a factor of 10, and set the output buffer size. The colorful name
for introduced high delay is "buffer bloat", leading to problems in some
applications like SSH and telephony, where the delay is visible to the user.

Section 2 of the Buffer sizing paper[2] is interesting and shows that if the TCP
connections are not synchronized, by the central limit theorem their aggregate
looks like a Gaussian, and it is possible to get away with smaller buffers.

\section*{Q\&A}

Q: Does the matrix for router traffic have zero diagonal? (line-cards don't
use the backplane to route traffic back to their own link)
A: No. bugs in routing protocol cause diagonal to be non-zero.

Q: Bandwidth apportioning according to weights in iSLIP.
A: One could also add the same input / output queue multiple times in the
round-robin schedule.

\section*{References}

1. Nick McKeown, The iSLIP Scheduling Algorithm for Input-Queued Switches, IEEE/ACM Trans. on Networking, 9(2), April 1999.

2. Guido Appenzeller, Isaac Keslassy, Nick McKeown, Sizing Router Buffers, SIGCOMM 2004.

\end{document}
