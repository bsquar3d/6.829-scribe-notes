\documentclass{6046}
\usepackage[pdftex,backref,colorlinks]{hyperref}

\title{6.829 - Computer Networks \\ Lecture 14 - Border Gateway Protocol (BGP)}
\author{Yevgeni Berzak, Andreea Gane}
\problemset{14}
\date{April Fools' Day 2013}

\begin{document}

\section*{Border Gateway Protocol (BGP)}

The BGP Internet routing system is based on the principle of Coopetition (Cooperative Competition).
There are about 20,000 networks called AS's (Autonomous Systems) that provide Internet service.
MIT is an example of an AS. One company may have several AS's.
 
There are two types of identifiers for an AS:
\begin{enumerate}
\item ASID - Autonomous System ID. 16 bit identifier ($1$ to $2^{16}$). 
			For instance MIT's ASID is 3. The identifier space is expected to be larger in the future.
\item IP prefix - IP addresses are 32 bit identifiers ($0$ to $2^{32} - 1$). Each autonomous system has 
			a range (or several ranges) of IP addresses, and every IP 
			belongs to one AS. IP addresses are allocated by an authority called ISNA.
      An IP prefix is denoted as A/m, where A is the prefix and m is its number of bits, such 
      that the prefix has $2^{32-m}$ addresses associated with it. 
      For example, MIT has the prefixes 18/8, 128.30/16 and others. These are non-contiguous ranges.
      The prefix 18/8 corresponds to the addresses 18.0.0.0 to 18.256.256.256.
      Using prefixes there is no need to maintain routes to every possible destination in MIT. 
      Instead, routers can store only prefixes and forward packets using the principle of 
      \emph{Longest Prefix Match}: choose the route corresponding to the longest prefix match to destination.  
      IP aggregation through prefixing is crucial for limiting the size of routing tables.  
\end{enumerate}

Intradomain Routing - routing withing an AS.
Our focus is on BGP Routing - between AS's. 
We will explain the fundamental relations of this protocol using Figure \ref{fig1}.

\begin{figure}[h]
\begin{center}
\includegraphics[width=.7\textwidth]{figs/fig1.png}
\caption{Main figure. Arrows from customers to transit providers represent money flow ($\$$), while 
arrows from providers to customers represent the sharing of routing tables (Rt). P links represent direct routes (peering exchanges).}\label{fig1}
\end{center}
\end{figure}

\subsection*{Transiting}
MIT buys Internet service from several providers. We denote this relation with an arrow going from 
customer C to provider Tp (Transit Provider). MIT has also its own Internet customers 
(e.g. CSAIL and other labs which are charged). Inside MIT there is an Intradomain Routing protocol
which we will not discuss here.

BGP is easier to understand in terms of money flow rather than packet flow. The commodity being 
payed for is the visibility of routing tables. For example, MIT pays Level 3 because it reveals to 
MIT the entries from its routing tables.

ISP's charge clients for bandwidth. Hence, to maximize traffic going through them, it's in their 
interest to reveal as much of their routing tables as possible to customers.

MIT must then load-balance its traffic by choosing between different ISP's for any given 
Internet destination. This process is called {\bf Ranking}. 
The converse of Ranking is {\bf Filtering}, namely choosing which subset
of the AS routes to advertise to other AS's. For example MIT reveals phone IP addresses
only to two VoIP ISP's.

\subsection*{Peering}
Assume MIT and Harvard have a large amount of traffic between them. In such case they may want to connect directly\footnote{In practice Research Institutions don't peer directly, but via a backbone network for educational purposes called Internet2}
instead of via other AS's, a relation called \emph{Peering} (denoted with P in the figure). 
Peering is advantageous as traffic flow will be more efficient (no intermediate stops in Virginia) and less 
money will be payed to intermediate ISP's. MIT has at least 17 peering relationships.

An important question in a peering relation is which routes should your border routers reveal 
to your peers? Answer - only routes to your customers (i.e. yourself). Revealing other routes 
means opening yourself to unwanted traffic (e.g. MIT wouldn't want to route Harvard's packets 
whose destination is Facebook). In particular, routes learned from a peering relationship are 
typically not revealed to other peers. If MIT is also peering with Stanford, it gets no benefit  
from routing Harvard's packets to Stanford.

Peering does not involve monetary exchange, and assumes symmetry of traffic in both directions. 
The acceptable ratio is typically up to 1:3. If a higher ratio is maintained over a long enough 
period of time, money is paid by the party that sends more.

The basic distinctions between transiting and peering are summarized in the following table.

\begin{tabular}{l*{1}{c}r}
                  & Transit & Peering  \\
\hline
Monetary Exchange & yes & no   \\
Routes Revealed    & all & customers  \\
\end{tabular}

\subsection*{Tier 1 ISP's}
There are several types of ISP's:
\begin{description}
\item[Tier 3] MIT scale, internal service.
\item[Tier 2] Comcast scale, a few states / regional.
\item[Tier 1] AT\&T scale, networks with no default routes. 
These are AS's that don't have network providers of their own, but 
have routes to all destinations in the Internet.
(As of a few years ago there are 9 such networks)
\end{description}

To allow access to the whole Internet, Tier 1 ISP's must peer with each other, 
forming a complete graph. A complete graph is needed because of the
definition of peering where only customers are revealed. For example in Figure \ref{fig2}, 
if AT\&T and Sprint wouldn't be connected they couldn't have reached each other's customers, as
L3 would reveal to them only its customers.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=.3\textwidth]{figs/fig2.png}
\caption{Tier 1 ISP's must peer with each other, forming a complete graph.}\label{fig2}
\end{center}
\end{figure}

Routing can be {\bf intransitive} (such that $A \rightarrow B, B \rightarrow C, A \nrightarrow C$)
For example, if there is Internet failure in Cambridge and we rely only on peering, we may have
the previously discussed case where MIT is peering with Harvard and BU but they don't 
peer with each other and hence cannot reach each other.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=.2\textwidth]{figs/fig3.png}
\caption{Asymmetric routing (example 1).}\label{fig3}
\end{center}
\end{figure}

Routing can also be {{\bf asymmetric}. We can have cases where $A \rightarrow B, B \nrightarrow A$.
For example consider Figure \ref{fig3}. 
A advertises routes to B, hence B can send packets to A. B doesn't advertise routes to A, but advertises to C expecting 
traffic from A to come through C. If for some reason C is no longer reachable from B, or C stops advertising its routes 
to A, we could end up in a situation where A cannot send to B.
30\%-50\% of internet routing is assymetric (route from A to B is different from B to A) not only on IP routing level 
but also on the AS level.

Another example is presented in Figure \ref{fig4}.
MIT gets primary service from L3 and backup service from Sprint.
Both L3 and Sprint advertise their routes to MIT. MIT advertises its routes only to L3.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=.35\textwidth]{figs/fig4.png}
\caption{Asymmetric routing (example 2).}\label{fig4}
\end{center}
\end{figure}

In the primary-backup model there is a trick which is used instead of waiting for a failure. This is
called AS path padding. For example in Figure \ref{fig4}, MIT may advertise its routes to both L3 and Sprint, but it 
advertises AS path 3 to L3 and 3333 to Sprint. Another router which gets both will always prefer the shorter AS path
3 over 3333 and hence send through L3 rather than Sprint.

Note that nothing stops Sprint from detecting the padding and advertising path 3 as well. To handle these cases, there is a research direction which uses signatures for the advertised paths, such that only untampered paths will be trusted.

See demo at \url{http://bgplay.routeviews.org}.

\end{document}
