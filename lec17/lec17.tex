\documentclass{6046}
\usepackage[pdftex,backref,colorlinks]{hyperref}

\title{6.829 - Computer Networks \\ Lecture 17 - BitTorrent is an Auction}
\author{Somak Das, Tiffany Chen}
\problemset{17}
\date{April 10, 2013}

\begin{document}

This work builds on BitTyrant. Designing incentives in BitTorrent fits in a larger problem in 
distributed systems: we want a certain behavior for all participants in the system, but they won't 
do it unless we motivate them. Most of the work here is described in the following paper: 
``BitTorrent is an Auction: Analyzing and Improving BitTorrent's Incentives,'' D. Levin, K. 
LaCurts, N. Spring, and B. Bhattacharjee, \textit{SIGCOMM 2008}.

\section*{Overview}
BitTorrent is a peer-to-peer system for sharing files. A \textbf{peer} is someone using BitTorrent. 
When he wants to download a particular file, he first contacts a lightweight centralized server 
called the \textbf{tracker}. The tracker sends back a list of IP addresses, corresponding to the 
peers who are also downloading that file. Then he tries to connect to a set of these peers. Some of 
them might not respond to him, but those who do and become connected are called \textbf{neighbors}. 
The subset of his neighbors to whom he sends data is called the \textbf{active set}. 
\textbf{Seeders} are peers that already have the entire file, but have stuck around. Collectively, 
all the peers downloading the same file are called the \textbf{swarm}.

The file is broken up into equally sized pieces (aka blocks or chunks). A peer gets these pieces in 
a random order (not necessarily sequential). Of course, he may prefer to these sequentially if he 
was streaming video from the file.

Why would a seeder stick around in a swarm, even when its download is done? BitTorrent has no 
incentives, but there are higher-layer, social (non-network) incentives. For example, private 
trackers are centralized communities that ban peers that have a poor seeding history. One can also 
imagine a karma system between different swarms that gives credit to seeders.

\subsection*{Phases of BitTorrent} The phases of BitTorrent relate to how much of the file has been 
downloaded. At the beginning, \textbf{bootstrapping} is getting the first pieces of the file. In 
the middle, \textbf{steady state} is trading pieces between peers. At the end, \textbf{endgame} is 
getting the last pieces of the file. There are different considerations and exploits for each state.

\section*{Steady State}

BitTyrant focuses on the steady state, in which the assumption is that peers have pieces to trade 
with other peers. A particular BitTorrent peer in steady state uploads to and downloads from 
different neighbors. How does he decide who to upload to, how much to upload, and why is this an 
important question (hint: it directly impacts performance)?

\subsection*{BitTorrent and BitTyrant Unchokers}

Let's break up the download time into $10$-second rounds. In round $t$, you (a peer) track who you 
are uploading to and downloading from. What happens in round $t$ informs what happens in round 
$t+1$. The BitTorrent unchoker picks the four peers who uploaded the most to you and gives them an 
equal share of your capacity. In this way, how much the peers upload to you impacts how much you 
give them back. Imagine your capacity is $40$; four peers are currently giving you $25$, $20$, 
$15$, and $10$; and an adversary joins with a capacity of $30$. He can give you his whole capacity 
and get back $10$ from you. But if he were smarter, he can give you just $11$ and be unchoked. 
Therefore, his best strategy is to just come in fourth place; this is what BitTyrant does -- it 
tries to come in last.

What happens if everyone in the swarm adopts BitTyrant's strategy? If everyone is selfish 
(withholding excess upload capacity), then the overall swarm capacity will go down. However, if 
everyone is strategic (not selfish), then the BitTyrant unchoker finds a way to maximize their 
upload capacities, so the swarm can see better performance.

The BitTorrent unchoker has another problem. Suppose the adversary from the previous example wants 
you to give him more than $10$. He can clone himself once, divide resources among himself, and have 
each clone give you $11$. Then his clones get back a total of $20$. The general name for this 
cloning attack is a \textbf{Sybil attack}, in which the attacker creates additional identities to 
subvert the system. (The names comes from a patient nicknamed Sybil who had multiple personality 
disorder.) In fact, many systems have to deal with Sybil attacks. On Twitter, people create 
multiple accounts to inflate their follower counts and make certain topics seem popular. On eBay, 
people create multiple accounts to inflate their rating. Clearly, we want systems to be 
\textbf{Sybil-proof}.

Goal: develop an unchoking strategy that is Sybil-proof and provides peers with incentives to 
upload as much as possible. There is already an incentive in BitTorrent to upload (if a peer 
doesn't upload, then he doesn't get anything back) but we can design a better system. To do so, 
let's frame the problem as auctions in game theory.

\subsection*{BitTorrent is an Auction}

A BitTorrent peer has a fixed number of equal goods: he uploads to four peers and give each an 
equal share of his capacity. The peers bid to him by uploading in round $t$, and he clears the 
auction (choosing the top four) in round $t+1$.

One idea is to instead hold a \textbf{proportional-share auction}, in which he gives to a peer in 
proportion to how much he got from the peer. The proportional share in round $t+1$ $\propto$ 
$(\text{the amount he got from a peer in round $t$}) / (\text{total amount he received})$. This 
allows for a variable number of unequal goods.

\subsection*{PropShare Unchoker}

The PropShare unchoker does exactly this. Instead of finding the top four, it looks at everyone. 
Imagine your capacity is $40$; four peers are currently giving you $30$, $25$, $20$, and $5$. You 
would give back $15$, $12.5$, $10$, and $2.5$, respectively. The adversary's first attack is no 
longer valid: if he uploads less, he receives less. This incentivizes him to upload more. The 
adversary's second (Sybil) attack is not valid either: if he clones himself, he still gets the same 
total. Therefore, PropShare is Sybil-proof.

Suppose you had global information of each peer's curve of bandwidth given to it vs.\ bandwidth 
received from it. Then your goal as a peer would be to maximize the bandwidth your receive from all 
peers subject to a fixed budget (your upload capacity), using the point of diminishing returns for 
each curve. Research shows that PropShare performs comparably to this strategy, despite having only 
local information.

\subsection*{Evaluation}

The PropShare authors conducted experiments in the PlanetLab testbed, which is a realistic (not 
simulation) network between educational institutions across the world. First, they set up a 
PlanetLab peer to join live swarms, testing one BitTorrent, BitTyrant, or PropShare vs.\ everyone 
else BitTorrent. Both BitTyrant and PropShare have better download times than BitTorrent, as 
expected. BitTyrant and PropShare perform comparably.

Next, they set up a swarm and varied the fraction of BitTyrant users (vs.\ BitTorrent). Again, 
BitTyrant peers are always faster than BitTorrent peers. But more BitTyrant peers meant worse 
performance for BitTyrant. (The implication is that if you use BitTyrant, then don't tell your 
friends about it!) They repeated this experiment for PropShare and showed that PropShare 
performance does not degrade as the fraction of PropShare users increases.

To recap steady state, a proportional-share strategy performs comparably to BitTyrant, incentivizes 
users to upload more, and is Sybil-proof.

\section*{Bootstrapping}
The assumption for bootstrapping is that peers have nothing to give other peers. The problem is 
that if a peer can't bid (because he has nothing), then he won't win any auction (and will continue 
to have nothing). To solve this, BitTorrent has a mechanism called \textbf{optimistic unchoking}, 
in which a peer randomly picks someone to ``be nice to.'' Every BitTorrent peer reserves a portion 
of their capacity to give freely to other peers. An adversary can exploit this by always pretending 
he has nothing and asking to be optimistically unchoked. There are no incentives to avoid this 
exploit, though that adversary would proceed very slowly.

One idea, called \textbf{junk updates}, is to force peers to upload (random) data. Here, the 
adversary is forced to upload something, so it might as well be real data. In general, this idea is 
called \textbf{proof of work}, because a new peer must prove he is willing to do the work 
(uploading data, even if it is junk). This method increases overhead since the bandwidth is being 
used to transmit useless information.The electronic currency Bitcoin uses this idea. 

But can we put new peers to work actually doing something useful? The idea is \textbf{triangle 
bootstrapping} (TBS). Imagine $A$ and $B$ are peers in steady state, and $N$ (bootstrapping) newly 
joins. A naive version of TBS has $A$ divert its traffic to $B$ through $N$, so that both $B$ and 
$N$ get data. Of course, if $N$ was adversarial, then it could just drop its traffic to $B$. One 
solution is to have $B$ tell $A$ what it has received. If $N$ was adversarial, $A$ would receive 
nothing from $B$, so $A$ detects $N$ is not forwarding the data and can block $N$. Another solution 
is to use encryption. $A$ encrypts what it sends to $N$, $N$ forwards that to $B$, and $B$ encrypts 
what it sends to $A$. If data authenticity checks pass, then $A$ sends the decryption key to $N$, 
$N$ forwards that to $B$, and $B$ sends the decryption key to $A$. An evaluation of TBS shows that 
TBS-enabled peers get their first 10 blocks faster than with regular BitTorrent or junk updates.

To recap bootstrapping, it is a small (initial) part of the download but it can still be exploited. 
A better bootstrapping mechanism like TBS can yield better performance throughout the download.

\section*{Endgame}
Two peers are mutually interesting if the first peer has blocks that the second one wants, and the 
second peer has blocks that the first peer wants. The assumption for endgame is that not many peers 
are mutually interesting.

One endgame exploit is \textbf{strategic piece revelation}. A strategic piece revealer wants to be 
as interesting to as many peers as possible. So, he strategically reveals pieces so that more peers 
are interested in him longer. For example, if he has pieces 1 and 2 and reveals both to peers $A$ 
and $B$, $A$ could get piece 1 and $B$ could get piece 2. But then $A$ and $B$ would trade within 
themselves instead of with him. Instead if he only reveals piece 1 to $A$ and $B$, both $A$ and $B$ 
have piece 1 and need piece 2 but they cannot trade within themselves to get it. Later he can 
reveal piece 2 as well. When a swarm has one strategic piece revealer, the fraction of peers with 
mutual interest to him ramps up very quickly and stays steady, so his download time is shorter than 
BitTorrent's. But if everyone is a strategic piece revealer, performance suffers because everyone 
is just lying.

To recap endgame, strategic piece revelation can improve the performance of an individual peer, but 
not many peers. Incentivizing peers to be truthful in this manner is an open problem.

\section*{Summary}

Despite its built-in incentives, there are lots of ways to exploit BitTorrent. It is possible to 
design incentive mechanisms that encourage peers to upload data while still keeping download times 
low. Designing incentives is a problem across the board for distributed systems.

\end{document}
